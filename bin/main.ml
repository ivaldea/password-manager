let build_output (is_flag, argument) =
  let prefix = if is_flag then "Got flag " else "Got argument " in
  prefix ^ argument ^ "\n"
;;

let rec print_args list =
  match list with
  | [] -> ()
  | element :: rest_of_list ->
    print_string @@ build_output element;
    print_args rest_of_list
;;

let get_actual_arguments =
  match Array.to_list Sys.argv with
  | [] -> []
  | _ :: actual_args -> actual_args
;;

let build_argument_tuple argument =
  let is_short_flag = String.starts_with ~prefix:"-" argument in
  let is_long_flag = String.starts_with ~prefix:"--" argument in
  if is_long_flag
  then true, String.sub argument 2 (String.length argument - 2)
  else if is_short_flag
  then true, String.sub argument 1 (String.length argument - 1)
  else false, argument
;;

let () = get_actual_arguments |> List.map build_argument_tuple |> print_args
